var precision = 0.0001;

function truncateDecimals(number, digits) {
    var multiplier = Math.pow(10, digits),
        adjustedNum = number * multiplier,
        truncatedNum = Math[adjustedNum < 0 ? 'ceil' : 'floor'](adjustedNum);

    return truncatedNum / multiplier;
};

module.exports = {

    /**
     * Returns region around passed damage based on precision variable.
     * Needed for querying db if any damage around passed damage was already detected
     * @param  {Damage} 
     * @return {Region}
     */
    getQueryRegion: function(damage) {
        var region = {};
        region.topLat = damage.lat + precision;
        region.bottomLat = damage.lat - precision;
        region.rightLon = damage.lon + precision;
        region.leftLon = damage.lon - precision;

        //we need to truncate unwanted decimal points, no to get coords like 50.3214000004
        for (var key in region) {
            region[key] = truncateDecimals(region[key], Math.log10(1/precision));
        }

        return region;
    },

    /**
     * Calculates average damageValue based on damageValue from entries from entryList
     * @param  {Entry} entryList List of entries 
     * @return {int} int 1-small damage, 2-medium damage, 3-big damage
     */
    getAverageDamageValue: function(entryList) {
        var total = 0;
        entryList.forEach(function(element, index) {
            total += element.damageValue;
        });
        return total / entryList.length;
    },

    /**
     * Returns new region object based on given string coords separated by separator
     * @param  {[string]} coords String coordinates separated by separator eg 50.023;18.120;49.0132;17.9901
     * @param  {[char]} separator 
     * @return {[Region]}
     */
    getRegionFromStringCoords: function(coords, separator) {
        var parts = coords.split(separator);
        return {
            topLat: parts[0],
            rightLon: parts[1],
            bottomLat: parts[2],
            leftLon: parts[3]
        };
    },

    cutPrecision: function(damage) {
        damage.lat = (Math.round(damage.lat / precision)) * precision;
    }
}
