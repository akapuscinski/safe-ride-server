var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var damageSchema = new Schema({
    lat: Number,
    lon: Number,
    damageValue: Number,
    entryCount: Number
});
var Damage = mongoose.model('Damage', damageSchema);

module.exports = {
    model: function() {
        return Damage;
    },

    create: function(entry, callback){
    	Damage.create({
    		lat: entry.lat,
    		lon: entry.lon,
    		damageValue: entry.damageValue,
    		entryCount: 1
    	}, callback);
    },

    findInRegion: function(region, callback){
    	Damage.find()
    		.where('lat').lt(region.topLat)
            .where('lat').gte(region.bottomLat)
            .where('lon').lt(region.rightLon)
            .where('lon').gte(region.leftLon)
            .exec(callback);
    }
}