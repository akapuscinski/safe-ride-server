var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var entrySchema = new Schema({
    lat: Number,
    lon: Number,
    damageValue: Number,
    accuracy: Number,
    speed: Number,
    xAcceleration: Number,
    yAcceleration: Number,
    zAcceleration: Number
});
var Entry = mongoose.model('Entry', entrySchema);

module.exports = {
    model: function() {
        return Entry;
    },

    create: function(entry, callback) {
        Entry.create({
            lat: entry.lat,
            lon: entry.lon,
            damageValue: entry.damageValue,
            accuracy: entry.accuracy,
            speed: entry.speed,
            xAcceleration: entry.xAcceleration,
            yAcceleration: entry.yAcceleration,
            zAcceleration: entry.zAcceleration
        }, callback);
    },

    getSimilarEntries: function(region, callback) {
        return Entry.find()
            .where('lat').lt(region.topLat)
            .where('lat').gte(region.bottomLat)
            .where('lon').lt(region.rightLon)
            .where('lon').gte(region.leftLon)
            .exec(callback);
    }
}
