//mongoose models
var Damage = require('./models/damage');
var Entry = require('./models/entry');
var damageHelper = require('./damage-helper');

module.exports = function(app) {

    app.get('/damages/:region', function(req, res) {
        var damages = Damage.findInRegion(damageHelper.getRegionFromStringCoords(req.params.region, ';'), function(err, damages){
        	res.json(damages);
        });
    });

    app.post('/damages', function(req, res) {
        //only in special cases when we want to manually modify existing damage e.g. when damage has been repaired
    });

    app.post('/entries', function(req, res) {
        //firstly add new entry to db
        Entry.create(req.body, function(err, entry) {
            if (err)
                res.send(err);
        });

        //check if there are other entries around new entry
        var region = damageHelper.getQueryRegion(req.body);
        Entry.getSimilarEntries(region, function(err, entries) {
            if (err)
                res.send(err);

            if (entries.length == 0) {
                //this is the first damage that has occurred in this place simply create new Damage record
                Damage.create(req.body, function(err, damage) {
                    if (err)
                        res.send(err);

                    res.send("New entry created, new damage created");
                });
            } else {
                //UPDATE DAMAGE VALUE
                //find damage for region
                Damage.findInRegion(region, function(err, damages) {
                    if (err)
                        res.send(err);

                    if (damages.length > 1)
                        res.code(500).send('New entry was added but when trying to add/update damate error occurred:'
                        + ' more than one damage was found inside region of new entry');
                    else {
                        //recalculate damage value based on entries near by
                        entries.push(req.body);
                        var newValue = damageHelper.getAverageDamageValue(entries);

                        var damage = damages[0];
                        var condition = { _id: damage['_id'] };
                        var update = { $set: { damageValue: newValue }, $inc: {entryCount: 1} };
                        Damage.model().update(condition, update, function(err, damage){
                        	if(err)
                        		res.send(err);

                        	res.end('New entry created, damage updated: damageValue ', newValue, ' count: ', entries.length);
                        });
                    }

                });

            }

        });
    });
};
