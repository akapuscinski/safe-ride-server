var chai = require('chai');
var expect = chai.expect; 
var damageHelper = require('../../app/damage-helper');

var damage = {lat: 50.00012, lon: 18.98012};
var precision = 0.0001; //todo should be immported from damage helper
var floatDeviation = precision;

describe('Damage helper tests', function(){

	it('Get query region for damage', function(){
		var region = damageHelper.getQueryRegion(damage);
		expect(region.topLat).to.be.closeTo(damage.lat+precision, floatDeviation);
		expect(region.bottomLat).to.be.closeTo(damage.lat-precision, floatDeviation);
		expect(region.leftLon).to.be.closeTo(damage.lon-precision, floatDeviation);
		expect(region.rightLon).to.be.closeTo(damage.lon+precision, floatDeviation);		
	});
});