var chai = require('chai');
var expect = chai.expect;
var Damage = require('./../../app/models/damage');

describe('Damage tests', function() {
    it('Should create new Damage', function(done) {
        var newDamage = {
            "lat": 50.21304,
            "lon": 18.76815,
            "damageValue": 1
        };
        Damage.create(newDamage, function(err, damage) {
            expect(err).to.not.exist;

            expect(damage._id).to.exist;
            expect(damage.entryCount).to.equal(1);
            expect(damage.lat).to.equal(newDamage.lat);
            expect(damage.lon).to.equal(newDamage.lon);
            expect(damage.damageValue).to.equal(1);
            done();
        })
    });

    it('Should increment entry count', function(done) {
        var newDamage = {
            "lat": 50.21304,
            "lon": 18.76815,
            "damageValue": 1
        };
        Damage.create(newDamage, function(err, damage) {
            var condition = { _id: damage['_id'] };
            var update = { $set: { damageValue: 3 }, $inc: { entryCount: 1 } };
            var options = { new: true }; //causes mongoose to return updated not old object
            Damage.model().update(condition, update, options, function(err, result) {
                expect(err).to.not.exist;

                expect(result.nModified).equal(1);

                Damage.model().findOne({
                    "lat": 50.21304,
                    "lon": 18.76815
                }, function(err, damage) {
                    expect(err).to.not.exist;

                    expect(damage._id).to.exist;
                    expect(damage.entryCount).to.equal(2);
                    expect(damage.lat).to.equal(newDamage.lat);
                    expect(damage.lon).to.equal(newDamage.lon);
                    expect(damage.damageValue).to.equal(3);
                    done();
                });
            });
        })
    });
});
