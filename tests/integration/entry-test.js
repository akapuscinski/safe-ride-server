var chai = require('chai');
var expect = chai.expect;
var Entry = require('./../../app/models/entry');

describe('Entry tests', function() {
    it('Should create new Entry record', function(done) {
        var testBody = {
            "lat": 50.21304,
            "lon": 18.76810,
            "damageValue": 1,
            "accuracy": 9,
            "speed": 15,
            "xAcceleration": 1.2,
            "yAcceleration": 4.1,
            "zAcceleration": 1.9
        };

        Entry.create(testBody, function(err, entry) {
            expect(err).to.not.exist;
            expect(entry._id).to.exist;
            expect(entry.lat).to.equal(testBody.lat);
            expect(entry.lon).to.equal(testBody.lon);
            expect(entry.damageValue).to.equal(testBody.damageValue);
            expect(entry.yAcceleration).to.equal(testBody.yAcceleration);
            done();
        });
    });

    it('Should find two similar entries', function(done) {
        var entry1 = {
            "lat": 50.21307,
            "lon": 18.76819,
            "damageValue": 1,
            "accuracy": 9,
            "speed": 15,
            "xAcceleration": 1.2,
            "yAcceleration": 4.1,
            "zAcceleration": 1.9
        };

        var entry2 = {
            "lat": 50.21304,
            "lon": 18.76810,
            "damageValue": 1,
            "accuracy": 9,
            "speed": 15,
            "xAcceleration": 1.2,
            "yAcceleration": 4.1,
            "zAcceleration": 1.9
        };

        var newEntry = {
            "lat": 50.21306,
            "lon": 18.76815,
            "damageValue": 1,
            "accuracy": 9,
            "speed": 15,
            "xAcceleration": 1.2,
            "yAcceleration": 4.1,
            "zAcceleration": 1.9
        };

        Entry.create(entry1, function(err, entry) {
            expect(err).to.not.exist;

            Entry.create(entry2, function(err, entry) {
                expect(err).to.not.exist;

                var region = require('../../app/damage-helper').getQueryRegion(newEntry);
                Entry.getSimilarEntries(region, function(err, entries) {
                    expect(entries.length).to.equal(2);
                    done();
                });

            })
        })

    });

    it('Should not find similar entries', function(done) {
        var entry1 = {
            "lat": 50.21310, //too much
            "lon": 18.76810,
            "damageValue": 1,
            "accuracy": 9,
            "speed": 15,
            "xAcceleration": 1.2,
            "yAcceleration": 4.1,
            "zAcceleration": 1.9
        };

        var entry2 = {
            "lat": 50.21300,
            "lon": 18.76820, //too much
            "damageValue": 1,
            "accuracy": 9,
            "speed": 15,
            "xAcceleration": 1.2,
            "yAcceleration": 4.1,
            "zAcceleration": 1.9
        };

        var newEntry = {
            "lat": 50.21300,
            "lon": 18.76810,
            "damageValue": 1,
            "accuracy": 9,
            "speed": 15,
            "xAcceleration": 1.2,
            "yAcceleration": 4.1,
            "zAcceleration": 1.9
        };

        Entry.create(entry1, function(err, entry) {
            expect(err).to.not.exist;

            Entry.create(entry2, function(err, entry) {
                expect(err).to.not.exist;

                var region = require('../../app/damage-helper').getQueryRegion(newEntry);
                Entry.getSimilarEntries(region, function(err, entries) {
                    expect(entries.length).to.equal(0);
                    done();
                });

            })
        })

    });
});
