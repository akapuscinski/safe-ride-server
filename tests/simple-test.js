function add(a,b){
	return a+b;
}

var chai = require('chai');
var expect = chai.expect; 

describe('simple tests', function(){
	it('simple add test', function(){
		expect(add(1,2)).to.equal(3);
	});
});